package com.javagda23.zad1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Wprowadź tekst:");
        String liniaTekstu = scanner.nextLine();

        String wynik = liniaTekstu.replaceAll(", ", " makarena ");
        System.out.println(wynik);
    }
}
