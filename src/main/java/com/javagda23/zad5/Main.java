package com.javagda23.zad5;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Wprowadź tekst:");
        String liniaTekstu = scanner.nextLine()
                .trim();

        // Abrakadabra - 4

        char[] znaki = liniaTekstu.toCharArray();
        char ostatniZnak = znaki[znaki.length - 1];

        int licznik = 0;
        for (char znak : znaki) {
            if (znak == ostatniZnak) {
                licznik++;
            }
        }

        System.out.println("Wynik: " + licznik);
    }
}
