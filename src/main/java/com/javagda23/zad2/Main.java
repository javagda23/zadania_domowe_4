package com.javagda23.zad2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Wprowadź tekst:");
        String liniaTekstu = scanner.nextLine();

        // zamiana tekstu na same małe litery i same wielkie litery
        String wynikU = liniaTekstu.toUpperCase();
        String wynikL = liniaTekstu.toLowerCase();
        System.out.println(wynikL);
        System.out.println(wynikU);
    }
}
